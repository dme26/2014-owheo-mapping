<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>Heatmap</Name>
        <UserStyle>
            <Title>Heatmap</Title>
            <Abstract>A heatmap surface showing population density</Abstract>
            <FeatureTypeStyle>
                <Transformation>
                    <ogc:Function name="gs:Heatmap">
                        <ogc:Function name="parameter">
                            <ogc:Literal>data</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>weightAttr</ogc:Literal>
                            <ogc:Literal>humidity</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>radiusPixels</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>radius</ogc:Literal>
                                <ogc:Literal>25</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>pixelsPerCell</ogc:Literal>
                            <ogc:Literal>10</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputBBOX</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_bbox</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputWidth</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_width</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputHeight</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_height</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                    </ogc:Function>
                </Transformation>
                <Rule>
                    <RasterSymbolizer>
                        <Geometry>
                            <ogc:PropertyName>the_geom</ogc:PropertyName></Geometry>
                        <Opacity>0.6</Opacity>
                        <ColorMap type="ramp">
                            <ColorMapEntry color="#FFFFFF" quantity=".000002" label="nodata"
                            opacity="0"/>
                            <ColorMapEntry color="#CCCFFF" quantity=".000005" label="500"/>
                            <ColorMapEntry color="#AAAFFF" quantity=".000015" label="1500"/>
                            <ColorMapEntry color="#666FFF" quantity=".00003" label="3000" />
                            <ColorMapEntry color="#111FFF" quantity=".0005" label="5000" />
                            <ColorMapEntry color="#0000FF" quantity=".00051" label="nodata" 
			    opacity="0"/>
                        </ColorMap>
                    </RasterSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
