<?xml version="1.0" encoding="UTF-8"?><sld:UserStyle xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml">
    <sld:Name>Default Styler</sld:Name>
    <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
            <ogc:Filter>
                <ogc:PropertyIsEqualTo>
                    <ogc:PropertyName>level</ogc:PropertyName>
                    <ogc:Literal>0</ogc:Literal>
                </ogc:PropertyIsEqualTo>
            </ogc:Filter>
            <sld:PolygonSymbolizer>
                <sld:Fill>
                    <sld:CssParameter name="fill">#003300</sld:CssParameter>
                </sld:Fill>
            </sld:PolygonSymbolizer>
            <sld:LineSymbolizer>
                <sld:Stroke/>
            </sld:LineSymbolizer>
            <sld:TextSymbolizer>
                <sld:Label>
                    <ogc:PropertyName>number</ogc:PropertyName>
                </sld:Label>
                <sld:Font>
                    <sld:CssParameter name="font-family">Times New Roman</sld:CssParameter>
                    <sld:CssParameter name="font-size">14</sld:CssParameter>
                    <sld:CssParameter name="font-style">normal</sld:CssParameter>
                    <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                </sld:Font>
                <sld:LabelPlacement>
                    <sld:PointPlacement>
                        <sld:AnchorPoint>
                            <sld:AnchorPointX>0.5</sld:AnchorPointX>
                            <sld:AnchorPointY>0.5</sld:AnchorPointY>
                        </sld:AnchorPoint>
                    </sld:PointPlacement>
                </sld:LabelPlacement>
                <sld:Fill/>
            </sld:TextSymbolizer>
        </sld:Rule>
        <sld:Rule>
            <ogc:Filter>
                <ogc:PropertyIsEqualTo>
                    <ogc:PropertyName>level</ogc:PropertyName>
                    <ogc:Literal>2</ogc:Literal>
                </ogc:PropertyIsEqualTo>
            </ogc:Filter>
            <sld:PolygonSymbolizer>
                <sld:Fill>
                    <sld:CssParameter name="fill">#00CC00</sld:CssParameter>
                </sld:Fill>
            </sld:PolygonSymbolizer>
            <sld:LineSymbolizer>
                <sld:Stroke/>
            </sld:LineSymbolizer>
            <sld:TextSymbolizer>
                <sld:Label>
                    <ogc:PropertyName>number</ogc:PropertyName>
                </sld:Label>
                <sld:Font>
                    <sld:CssParameter name="font-family">Times New Roman</sld:CssParameter>
                    <sld:CssParameter name="font-size">14</sld:CssParameter>
                    <sld:CssParameter name="font-style">normal</sld:CssParameter>
                    <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                </sld:Font>
                <sld:LabelPlacement>
                    <sld:PointPlacement>
                        <sld:AnchorPoint>
                            <sld:AnchorPointX>0.5</sld:AnchorPointX>
                            <sld:AnchorPointY>0.5</sld:AnchorPointY>
                        </sld:AnchorPoint>
                    </sld:PointPlacement>
                </sld:LabelPlacement>
                <sld:Fill/>
            </sld:TextSymbolizer>
        </sld:Rule>
        <sld:Rule>
            <ogc:Filter>
                <ogc:And>
                    <ogc:And>
                        <ogc:Not>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>level</ogc:PropertyName>
                                <ogc:Literal>0</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:Not>
                        <ogc:Not>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>level</ogc:PropertyName>
                                <ogc:Literal>1</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:Not>
                    </ogc:And>
                    <ogc:Not>
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>level</ogc:PropertyName>
                            <ogc:Literal>2</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Not>
                </ogc:And>
            </ogc:Filter>
            <sld:PolygonSymbolizer>
                <sld:Fill>
                    <sld:CssParameter name="fill">#d3d3d3</sld:CssParameter>
                </sld:Fill>
            </sld:PolygonSymbolizer>
            <sld:LineSymbolizer>
                <sld:Stroke/>
            </sld:LineSymbolizer>
            <sld:TextSymbolizer>
                <sld:Label>
                    <ogc:PropertyName>number</ogc:PropertyName>
                </sld:Label>
                <sld:Font>
                    <sld:CssParameter name="font-family">Times New Roman</sld:CssParameter>
                    <sld:CssParameter name="font-size">14</sld:CssParameter>
                    <sld:CssParameter name="font-style">normal</sld:CssParameter>
                    <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                </sld:Font>
                <sld:LabelPlacement>
                    <sld:PointPlacement>
                        <sld:AnchorPoint>
                            <sld:AnchorPointX>0.5</sld:AnchorPointX>
                            <sld:AnchorPointY>0.5</sld:AnchorPointY>
                        </sld:AnchorPoint>
                    </sld:PointPlacement>
                </sld:LabelPlacement>
                <sld:Fill/>
            </sld:TextSymbolizer>
        </sld:Rule>
        <sld:Rule>
            <ogc:Filter>
                <ogc:PropertyIsEqualTo>
                    <ogc:PropertyName>level</ogc:PropertyName>
                    <ogc:Literal>1</ogc:Literal>
                </ogc:PropertyIsEqualTo>
            </ogc:Filter>
            <sld:PolygonSymbolizer>
                <sld:Fill>
                    <sld:CssParameter name="fill">#FFFFFF</sld:CssParameter>
                </sld:Fill>
            </sld:PolygonSymbolizer>
            <sld:LineSymbolizer>
                <sld:Stroke/>
            </sld:LineSymbolizer>
            <sld:TextSymbolizer>
                <sld:Label>
                    <ogc:PropertyName>number</ogc:PropertyName>
                </sld:Label>
                <sld:Font>
                    <sld:CssParameter name="font-family">Times New Roman</sld:CssParameter>
                    <sld:CssParameter name="font-size">14</sld:CssParameter>
                    <sld:CssParameter name="font-style">normal</sld:CssParameter>
                    <sld:CssParameter name="font-weight">normal</sld:CssParameter>
                </sld:Font>
                <sld:LabelPlacement>
                    <sld:PointPlacement>
                        <sld:AnchorPoint>
                            <sld:AnchorPointX>0.5</sld:AnchorPointX>
                            <sld:AnchorPointY>0.5</sld:AnchorPointY>
                        </sld:AnchorPoint>
                    </sld:PointPlacement>
                </sld:LabelPlacement>
                <sld:Fill/>
            </sld:TextSymbolizer>
        </sld:Rule>
    </sld:FeatureTypeStyle>
</sld:UserStyle>
