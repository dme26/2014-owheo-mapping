#!/usr/bin/env bash

###############################################################################
# This script installs Curl, PostgreSQL, GEOS & PostGIS extensions, osm2pgsql #
###############################################################################

#-------------------------------CURL-------------------------------

#install curl
sudo apt-get install -y curl

#-------------------------------POSTGRESQL-------------------------------

#install postgresql and libraries
sudo apt-get install -y postgresql-9.1
sudo apt-get install -y postgresql-contrib
sudo apt-get install -y build-essential postgresql-server-dev-9.1 libxml2-dev libproj-dev libjson0-dev xsltproc docbook-xsl libgdal1-dev

#-------------------------------GEOS-------------------------------

#install geos
wget http://download.osgeo.org/geos/geos-3.3.8.tar.bz2
tar xvfj geos-3.3.8.tar.bz2
cd geos-3.3.8
./configure
make
sudo make install
cd ..

#-------------------------------POSTGIS-------------------------------

#install postgis
wget http://download.osgeo.org/postgis/source/postgis-2.0.3.tar.gz
tar xvfz postgis-2.0.3.tar.gz
cd postgis-2.0.3
./configure
make
sudo make install
sudo ldconfig
sudo make comments-install

#replace pg_hba.conf for enabling access to postgresql
#if not replaced, default conf will refuse connexions
sudo cp /vagrant/pg_hba.conf /etc/postgresql/9.1/main/pg_hba.conf

#-------------------------------OSM2PGSQL-------------------------------

#install Osm2pgsql libraries
sudo apt-get install -y build-essential libgeos++-dev libpq-dev libbz2-dev proj libtool automake git

#install osm2pgsql
sudo apt-get install -y osm2pgsql
