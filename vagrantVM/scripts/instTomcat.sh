#! /usr/bin/env bash

############################################
# This script installs OpenJDK and Tomcat7 #
############################################

#install OpenJDK
#OpenJDK is the only way to link tomcat7 to JAVA with Ubuntu 12+
sudo apt-get install -y openjdk-7-jre openjdk-7-jdk

#set java_home
sudo echo "export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64" >> ~/.bashrc

#install Tomcat7
sudo apt-get install -y tomcat7
sudo chmod -R 777 /usr/share/tomcat7

#set commands start/stop/restart
sudo echo -e "export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64 \ncase $1 in \nstart) \nsh /usr/share/tomcat7/bin/startup.sh \n;; \nstop) \nsh /usr/share/tomcat7/bin/shutdown.sh \n;; \nrestart) \nsh /usr/share/tomcat7/bin/shutdown.sh \nsh /usr/share/tomcat7/bin/startup.sh \n;; \nesac \nexit 0" >> /etc/init.d/tomcat7

#set rights and create necessary links
sudo chmod 777 /etc/init.d/tomcat7
sudo ln -s /etc/init.d/tomcat7 /etc/rc1.d/K99tomcat
sudo ln -s /etc/init.d/tomcat7 /etc/rc2.d/S99tomcat
sudo chmod -R 777 /etc/tomcat7
sudo mkdir /usr/share/tomcat7/logs
sudo chmod 777 /usr/share/tomcat7/logs
/etc/init.d/tomcat7 start
