#!/bin/bash

#install apache2 and php5
sudo apt-get install -y apache2 automake
sudo apt-get install -y libapache2-mod-php5 php5 php5-common php5-curl php5-dev php5-gd php5-idn php-pear php5-imagick php5-imap php5-json php5-mcrypt php5-memcache php5-mhash php5-ming php5-mysql php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl

#copy gifs app directory into apache
sudo cp -R /vagrant/gifs /var/www
sudo service apache2 reload

#enable gifs app to be deployed on the server
sudo cp /vagrant/engifs /etc/apache2/sites-available
a2ensite engifs

#enable php parsing in html
echo 'AddHandler application/x-httpd-php .html .htm' >> /etc/apache2/mods-enabled/php5.conf
sudo service apache2 restart

sudo chmod 777 -R /var/www/gifs/
