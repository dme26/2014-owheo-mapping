#! /usr/bin/env bash

##########################################################
# This script creates DB and inserts data with osm2pgsql #
##########################################################

#restarting postgresql
sudo /etc/init.d/postgresql restart

#copying default.style with new columns
#new columns are mainly for data and timestamp
sudo cp /vagrant/osmdb/default.style /usr/share/osm2pgsql/default.style
sudo cp /vagrant/osmdb/defaultNode.style /usr/share/osm2pgsql/defaultNode.style

#give rights
sudo chmod 777 -R /vagrant/
sudo chmod 777 -R /usr/share/osm2pgsql
sudo chmod 777 -R /usr/share/postgresql

#create template and extension hstore
sudo -u postgres createdb template_owheo
psql -U postgres -h localhost -d template_owheo -c "CREATE EXTENSION hstore;"

#enabling gis DB & correct postgis2 compatibilities
psql -U postgres -h localhost -d template_owheo -f /usr/share/postgresql/9.1/contrib/postgis-2.0/postgis.sql
psql -U postgres -h localhost -d template_owheo -f /usr/share/postgresql/9.1/contrib/postgis_comments.sql
psql -U postgres -h localhost -d template_owheo -f /usr/share/postgresql/9.1/contrib/postgis-2.0/spatial_ref_sys.sql
psql -U postgres -h localhost -d template_owheo -f /usr/share/postgresql/9.1/contrib/postgis-2.0/legacy.sql 
psql -U postgres -h localhost -d template_owheo -f /usr/share/postgresql/9.1/contrib/postgis-2.0/legacy_minimal.sql
psql -U postgres -h localhost -d template_owheo -f /usr/share/postgresql/9.1/contrib/postgis-2.0/legacy_gist.sql

#create DBs following template
sudo -u postgres createdb Owheo -T template_owheo
sudo -u postgres createdb Nodes -T template_owheo

#insert data from osm files with columns set on *.style files
osm2pgsql -U postgres -s -S /usr/share/osm2pgsql/default.style --hstore -d Owheo /vagrant/osmdb/Owheo.osm
osm2pgsql -U postgres -s -S /usr/share/osm2pgsql/defaultNode.style --hstore -d Nodes /vagrant/osmdb/Nodes.osm

#rename useful tables to use them easier
psql -U postgres -h localhost -d Owheo -c "ALTER TABLE planet_osm_polygon RENAME TO Owheo;"
psql -U postgres -h localhost -d Nodes -c "ALTER TABLE planet_osm_point RENAME TO Nodes;"

#create column with timestamptz type to store dates compatible with geoserver
psql -U postgres -d Nodes -c "ALTER TABLE nodes ADD COLUMN timestamp timestamptz null;"

#delete points that are not nodes
psql -U postgres -d Nodes -c "DELETE FROM nodes WHERE node_id IS NULL;"

#fake node to init max data for scale - update values for this nodes created with JOSM
psql -U postgres -d Nodes -c "update nodes set temperature = 1000000, light = 1000000, humidity = 100000000 where node_id = 9999;"

#indexing columns
psql -U postgres -d Nodes -c "CREATE INDEX ind_nodes_id ON nodes (node_id);"
psql -U postgres -d Nodes -c "CREATE INDEX ind_nodes_way ON nodes (way);"
psql -U postgres -d Nodes -c "CREATE INDEX ind_nodes_tem ON nodes (temperature);"
psql -U postgres -d Nodes -c "CREATE INDEX ind_nodes_hum ON nodes (humidity);"
psql -U postgres -d Nodes -c "CREATE INDEX ind_nodes_lgt ON nodes (light);"
psql -U postgres -d Nodes -c "CREATE INDEX ind_nodes_tim ON nodes (timestamp);"

